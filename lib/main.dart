import 'package:flutter/material.dart';
import 'package:flutter_submodule/module/counter/lib/my_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: Center(
          child: MyText(),
        ),
      ),
    );
  }
}

